 <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
 
 <html>
  <head>
<meta charset="ISO-8859-1">
<title>Login</title>
<link rel="stylesheet" type="text/css" href="/HostelFinder/css/drawer.css">

<link rel="stylesheet" href="/HostelFinder/bs/bootstrap.min.css">
<script src="/HostelFinder/bs/jquery-3.3.1.min.js"></script>
<script src="/HostelFinder/bs/popper.min.js"></script>
<script src="/HostelFinder/bs/bootstrap.min.js"></script>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  
  <style  >
  
   
  
  </style>
  </head>
  <body><div class="area">
  
  
  </div>
  <nav class="main-menu" style="margin-top: 100px">
            <ul>
                <li>
                    <a href="../owner/ownerhome.jsp">
                        <i class="fa fa-home fa-2x"></i>
                        <span class="nav-text">
                           HOME
                        </span>
                    </a>
                  
                </li>
                
              
                
                
                
                <li class="has-subnav">
                    <a href="../owner/ownerprofile.jsp">
                        <i class="fa fa-eye fa-2x"></i>
                        <span class="nav-text">
                           VIEW PROFILE
                        </span>
                    </a>
                                    <li>
                    <a href="../owner/editownerprofile.jsp">
                        <i class="fa fa-edit fa-2x"></i>
                        <span class="nav-text">
                         EDIT PROFILE
                        </span>
                    </a>
                </li>
                    
                    
                 
              <li class="has-subnav">
                   
                     <a><i  class="fa fa-folder-open fa-2x"></i>
                      <span class="nav-text" style="color: grey"
                          class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">UPLOAD PIC
                        </span>
                
                   </a>
                </li> 
                
                
                
                   <!-- <li class="has-subnav">
                    <a href="../owner/ownerpic.jsp">
                       <i class="fa fa-folder-open fa-2x"></i>
                        <span class="nav-text">
                            UPLOAD PIC
                        </span>
                    </a>
                   
                </li>
                 -->
                
                    <li class="has-subnav">
                    <a href="/HostelFinder/jsp/inbox.jsp">
                       <i class="fa fa-inbox fa-2x"></i>
                        <span class="nav-text">
                            INBOX
                        </span>
                    </a>
                    
                </li>
                
                <li>
                    <a href="/HostelFinder/jsp/sendmessage.jsp">
                        <i class="fa fa-inbox fa-2x"></i>
                        <span class="nav-text">
                          SEND ITEM
                        </span>
                    </a>
                </li>
                <li>
                   <a href="/HostelFinder/jsp/compose.jsp">
                       <i class="fa fa-share-square fa-2x"></i>
                        <span class="nav-text">
                          COMPOSE
                        </span>
                    </a>
                </li>
                
                 <li>
                   <a href="/HostelFinder/jsp/owner/addhostel.jsp">   
                   
                   
                   <i class="fa fa-hotel"></i>
                        <span class="nav-text">
                          ADD HOSTEL
                        </span>
                    </a>
                </li>
                
                
                 <li>
                    <!--  <a href="/HostelFinder/jsp/owner/addhostel.jsp">   -->
                   
                  <a href="/HostelFinder/jsp/owner/addroom.jsp">
                   <i class="fa fa-hotel"></i>
                        <span class="nav-text">
                          ADD ROOM
                        </span>
                    </a>
                </li>
                 
            </ul>

            <ul class="logout">
                <li>
                   <a href="/HostelFinder/LogOff">
                         <i class="fa fa-power-off fa-2x"></i>
                        <span class="nav-text">
                            Logout
                        </span>
                    </a>
                </li>  
            </ul>
        </nav>
        
        
        
         <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
           
          <h4 class="modal-title">Upload Your Pic To Atttract The Eyes...</h4>
        </div>
        <div class="modal-body">
         <div class="box">
 
	<form method="post" action="/HostelFinder/Ownerpic"
		enctype="multipart/form-data">
		<input type="file" name="fileupload">
		<button type="submit" style="margin-left: 270px;"><b>UPLOAD</b></button>
	</form>
	</div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
 
        
  </body>
    </html>