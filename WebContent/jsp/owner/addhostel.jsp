   <%@page import="java.sql.ResultSet"%>
<%@page import="ehotel.dbtask.CrudOperation"%>
 
<%@page import="ehotel.beans.*"%>
<%@page import="ehotel.servlet.*" %>

  <%@page import="java.util.*"%>
  <%@page import="ehotel.dbtask.*" %>
 <!DOCTYPE html>
<html> 
<head>
<meta charset="ISO-8859-1">
<title>Add hostel</title>


  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
 
<link rel="stylesheet" type="text/css" href="/HostelFinder/fontawesome/css/all.css">  
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">





<style>
@font-face {
	font-family: "myfont";
	src: url("/HostelFinder/font/Welcome.ttf")
}
 .fa {
  
  font-size: 30px;
  width: 30px;
   
   
   
   
  border-radius: 50%;
  color: white;
}

.fa:hover {

cursor: pointer;
color: yellow;
}
 

</style>


</head>
<body >
<%
 



 

 HttpSession hs=request.getSession(false);
 String id=(String)hs.getAttribute("sessionkey");                  
 if(id==null ||hs.isNew())
 {
	request.setAttribute("msg", ErrorMessage.AUTHENICATIONMSG);
	 RequestDispatcher rd=request.getRequestDispatcher("/jsp/login.jsp");
	 rd.forward(request, response);
	 
	 
	 
	 
	 
 
 }
 
 else{
	 
	 System.out.println("session in jsp"+hs.getId());
	  
	 long time =hs.getLastAccessedTime();
	 Calendar cl=Calendar.getInstance(); 
     cl.setTimeInMillis(time);
     int minute=cl.get(Calendar.MINUTE);
     int hour=cl.get(Calendar.HOUR);
     int seconds=cl.get(Calendar.SECOND);
     int day=cl.get(Calendar.DAY_OF_MONTH);

     
     
	System.out.println("session  last accessed by "+id);
	System.out.println("hour is"+hour+"minute is"+minute+"second is"+seconds);
	
	System.out.println("day is"+day);
	 
	 
%>  

 




<div
		style="width: 100%; height: 100px; float: left; background-color: black">
		
		<i class="fab fa-rockrms"
			style="font-size: 70px; color: gold; margin-top: 10px"></i>  <span
			
			
			style="margin-left: 35%; font-family: myfont; font-size: 90px; color: gold">TAVERN
			&nbsp;<em style="font-size:40px;">--->feels like home</em></span>

 


	</div>
  
 
  
  
 



  
                 
  <jsp:include page="../owner/ownerdrawer.jsp"></jsp:include>   



 

<div style="width: 100%;height:400px;background-color:white;float:left">

 <div style="margin-left: 500px ;margin-top: 70px" class="vp" >
				<div class="reg-wrapper">
					<form action="/HostelFinder/AddHstl" method="POST" class="regform"
						onsubmit="return main()">
						<table>
							<tr>
								<td>
									<div class="input-wrap">
										<div class="labelwrap">
											<i class="fas fa-h-square" style="color: black"  ></i> <label
												for="exthname">Hostel Name</label>
										</div>
										<div class="inputbox">
											<input type="text" id="txthname" name="txthname"
												onclick="dhname()">
										</div>
										<span id="mshname"></span>
									</div>
								</td>
								<td>
									<div class="input-wrap" style="margin-left: 10px">
										<div class="labelwrap">
											<i class="fas fa-phone-square-alt" style="color: black"></i> <label
												for="txtphone">Phone</label>
										</div>
										<div class="inputbox">
											<input type="number" id="txtphone" name="txtphone"
												onclick="dphone()">
										</div>
										<span id="msphone"></span>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="input-wrap">
										<div class="labelwrap">
											<i class="far fa-building" style="color: black"></i> <label
												for="cmbcity">City</label>
										</div>
										<div class="inputbox">
											<select name="cmbcity" id="cmbcity" >
												<option value="default">City</option>
												<%
													String strc="select * from cityinfo";
                                                    ResultSet rs=CrudOperation.getData(strc);
                                                    while(rs.next()){
                                                    	String cname=rs.getString("cityid");
                                                    	String ciname=rs.getString("cityname");
												%>
												<option value="<%=cname %>"><%=ciname %></option>
												<%} %>
											</select>
										</div>
										<span id="mscity"></span>
									</div>
								</td>
								<td>
									<div class="input-wrap">
										<div class="labelwrap">
											<i class="fas fa-location-arrow" style="color: black"></i> <label
												for="txtarea">Area Pincode</label>
										</div>
										<div class="inputbox">
											<input type="number" id="txtarea" name="txtarea"
												onclick="darea()">
										</div>
										<span id="msarea"></span>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="input-wrap">
										<div class="labelwrap">
											 <i class="fas fa-forward" style="color: black"></i> <label
												for="category">Category</label>
										</div>
										<div class="gender">
											<input type="radio" name="category" id="rdhostel"
												value="hostel" onclick="dcategory()"> <label
												for="rdhostel">Hostel</label> <input type="radio"
												name="category" id="rdpg" value="pg" onclick="dcategory()">
											<label for="rdpg">PG</label>
										</div>
										<span id="mscategory"></span>
									</div>
								</td>
								<td>
									<div class="input-wrap">
										<div class="labelwrap">
											 <i class="fas fa-share-alt" "style="color: black"></i> <label
												for="for">For</label>
										</div>
										<div class="gender">
											<input type="radio" name="for" id="rdboys" value="boys"
												onclick="dfor()"> <label for="rdboys">Boys</label> <input
												type="radio" name="for" id="rdgirls" value="girls"
												onclick="dfor()"> <label for="rdgirls">Girls</label>
										</div>
										<span id="msfor"></span>
									</div>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<div class="input-wrap">
										<div class="labelwrap">
											<i class="fas fa-map-marked-alt" style="color: black"></i> <label
												for="txtaddress">Address</label>
										</div>
										<div class="inputbox">
											<textarea name="txtaddress" id="txtaddress"
												onclick="daddress()"></textarea>
										</div>
										<span id="msaddress"></span>
									</div>
								</td>
							</tr>
						</table>
						<div class="btn">
							<button type="reset">Reset</button>
				<button style="color: black;" type="submit">Submit</button>
							<br> <br> <br>
						</div>
					</form>
					
					</div>
					</div>
					</div>
					
<div style="width: 100%;height: 100px;background-color:#dfbd00;float: left;margin-top: 100px">
 
			<a style="font-size: 30px;color: white; margin-left: 42%;" href=""><strong>TAVERN HOSTEL</strong></a>
		 
 
		<div style="margin-left: 100px;">
		 
		        <a href="#" class="fa fa-facebook"></a>
				<a href="#" class="fa fa-twitter"></a>
				<a href="#" class="fa fa-whatsapp"></a>
				 
				 
				<a href="#" class="fa fa-instagram"></a>
					 </div>
				<div style="margin-top: -30px">
				 <a style=" color: white; margin-left: 700px; font-size: 20px;" href="">About us</a>
				  
				   <a style=" color: white; margin-left: 500px; font-size: 20px" href="">Contact us</a>
</div>


</div>

   <%} %> 
</body>
</html>


 