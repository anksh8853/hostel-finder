package ehotel.beans;

import java.sql.*;


import java.util.ArrayList;

import javax.management.loading.PrivateClassLoader;

import ehotel.dbtask.CrudOperation;

public class Message {
	
	
	private int msgid;
	private String senderid,receiverid,subject,msgtext;
	private String rstatus,sstatus;
	
	
	 
	
	private static Connection con;
	private static PreparedStatement ps;
	private static ResultSet rs;
	
	
	
	
	
	public static ArrayList getMessage( String sql,String userid)
	{
		
		
		ArrayList<Message>msgList=new ArrayList<Message>();   // declare array list
		
		con=CrudOperation.createConnection();
		
		try {
			
			 
			ps=con.prepareStatement(sql);
			ps.setString(1,userid );
			rs=ps.executeQuery();
			
			
			while(rs.next())
			{
				int id=rs.getInt("msgid");
				String senderid=rs.getString("senderid");
				Date d=rs.getDate("date");
				String subject=rs.getString("subject");
				String msg=rs.getString("message");
				String rstatus=rs.getString("rstatus");
				String sstatus=rs.getString("sstatus");
				String receiverid=rs.getString("receiverid");
				 
				 Message m=new Message(id, senderid, receiverid, subject, msg, d, rstatus, sstatus);
				
				
			
				msgList.add(m);
			}
			
			
		}
		catch (SQLException e) {
			 System.out.println(e);
		}
		return msgList;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public Message(int msgid, String senderid, String receiverid, String subject, String msgtext, Date date,
			String rstatus, String sstatus) {
		 
		this.msgid = msgid;
		this.senderid = senderid;
		this.receiverid = receiverid;
		this.subject = subject;
		this.msgtext = msgtext;
		this.date = date;
		this.rstatus = rstatus;
		this.sstatus = sstatus;
	}

	private Date date;
	
	public int getMsgid() {
		return msgid;
	}

	public void setMsgid(int msgid) {
		this.msgid = msgid;
	}

	public String getSenderid() {
		return senderid;
	}

	public void setSenderid(String senderid) {
		this.senderid = senderid;
	}

	public String getReceiverid() {
		return receiverid;
	}

	public void setReceiverid(String receiverid) {
		this.receiverid = receiverid;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMsgtext() {
		return msgtext;
	}

	public void setMsgtext(String msgtext) {
		this.msgtext = msgtext;
	}

	public String getRstatus() {
		return rstatus;
	}

	public void setRstatus(String rstatus) {
		this.rstatus = rstatus;
	}

	public String getSstatus() {
		return sstatus;
	}

	public void setSstatus(String sstatus) {
		this.sstatus = sstatus;
	}

	
	
	
	

}
