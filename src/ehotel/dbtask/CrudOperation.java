package ehotel.dbtask;

import java.sql.*;
import java.util.*;

import org.apache.jasper.tagplugins.jstl.core.Catch;
public class CrudOperation 
{
   public static Connection con;
   public static ResourceBundle rb;
   
   public static PreparedStatement ps;
   public static ResultSet rs;
   
   
   public static ResultSet getdata(String query,int id)
   {
	   
	   try {
			  
		       con=createConnection(); 
			  ps=con.prepareStatement(query);
		      ps.setInt(1, id);
		   rs=   ps.executeQuery();
			  
		  }catch (SQLException se) {
			 System.out.println(se);
		}
		  return rs;  
	   
	   
	   
   }
   
   
   public static ResultSet getdata(String query,String id)
   {
	   
	   try {
			  
		       con=createConnection(); 
			  ps=con.prepareStatement(query);
		      ps.setString(1, id);
		   rs=   ps.executeQuery();
			  
		  }catch (SQLException se) {
			 System.out.println(se);
		}
		  return rs;  
	   
	   
	   
   }
   
   public static ResultSet getData(String query)
   {
	   
	  try {
		  
		  con=createConnection();
		  ps=con.prepareStatement(query);
	   rs=ps.executeQuery();
		  
	  }catch (SQLException se) {
		 System.out.println(se);
	}
	  return rs;
	   
   }
   public static Connection createConnection()
   {
   try {
	   rb=ResourceBundle.getBundle("ehotel/properties/dbinfo");   
	   String userid= rb.getString("web.userid");
	   System.out.println(userid);
	   
	   String slogan=rb.getString("web.slogan");
	   System.out.println(slogan);
	   
	   Class.forName("com.mysql.jdbc.Driver");   
	   
		 
	   
	   
	   con=DriverManager.getConnection(rb.getString("web.url"),userid,rb.getString("web.userpass"));
	 
   }
   
   
   catch(ClassNotFoundException|SQLException se)
   
   {
	  System.out.println(se); 
	   
	   
   }
   return con;
}
   
	
	 
	 
}