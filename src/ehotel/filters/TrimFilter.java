package ehotel.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

/**
 * Servlet Filter implementation class TrimFilter
 */
@WebFilter({ "/TrimFilter" ,"/Login"})
public class TrimFilter implements Filter {

    /**
     * Default constructor. 
     */
    public TrimFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		// place your code here

	HttpServletRequest req=	(HttpServletRequest)request;
		// pass the request along the filter chain
		String userid=req.getParameter("txtuserid");
		userid=userid.trim().toLowerCase();
		req.setAttribute("uid", userid);
		
		System.out.println("filter called");
		chain.doFilter(req, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
		
		System.out.println("filter called");
	}

}
