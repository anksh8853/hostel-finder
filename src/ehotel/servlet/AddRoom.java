package ehotel.servlet;

import java.io.IOException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ehotel.beans.ErrorMessage;
import ehotel.beans.duplicateroommsg;
import ehotel.dbtask.CrudOperation;

/**
 * Servlet implementation class AddHstl
 */
@WebServlet("/AddRoom")
public class AddRoom extends HttpServlet {
	private static final long serialVersionUID = 1L;
	PreparedStatement ps,pscheck;
	Connection con;
	ResultSet rs;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddRoom() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession hs=request.getSession(false);
    	String id=(String)hs.getAttribute("sessionkey");

    	String hid=request.getParameter("txthid");
		String typeid=request.getParameter("cmbroomtype");
		String charges=request.getParameter("txtcharges");
		String trooms=request.getParameter("txttotalrooms");
		String cooler=request.getParameter("cooler");
		String ac=request.getParameter("ac");
		String details=request.getParameter("txtotherdetails");
		
		try {
			con=CrudOperation.createConnection();
		 
			
			String strin="insert into roomdetails(typeid, charges, totalrooms, cooler, ac, otherdetails,  hid) values(?,?,?,?,?,?,?)";
		 
			
			
			ps=con.prepareStatement(strin);
			//	ps.setString(1, hid);
			
					ps.setString(1, typeid);
					ps.setString(2, charges);
					ps.setString(3, trooms);
					ps.setString(4, cooler);
					ps.setString(5, ac);
					ps.setString(6, details);
					 
					ps.setString(7, hid);
			System.out.println(ps);
			int rw=ps.executeUpdate();
			
			if(rw>0 ) {
				response.sendRedirect("/HostelFinder/jsp/owner/addhostel.jsp");
			}
			
		}catch(SQLException se) {
			System.out.println(se);
		}
		finally {
			try {
				if(ps!=null)ps.close();
			}catch(SQLException se) {
				System.out.println(se);
			}
		}
	}

}
