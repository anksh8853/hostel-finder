package ehotel.servlet;

import java.io.IOException;
import java.sql.*;
import java.util.Calendar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ehotel.beans.ErrorMessage;
import ehotel.dbtask.CrudOperation;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private Connection con;
	private PreparedStatement pslogin;
	private ResultSet rs;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at:").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		
		
		       
		 
		    
		       String userid=(String)request.getAttribute("uid");
		       
		       System.out.println("userid in servlet"+userid);
		       
		         String userpass=request.getParameter("txtuserpass");
		         
		         System.out.println("userpass in servlet"+userpass);
		 
		        	  
		        	  try {
		        		  con=CrudOperation.createConnection();  
		        		  String  strsql="select * from logindetails where userid=? and userpass=?";
		        		  
		        		  pslogin=con.prepareStatement(strsql); 
		        		  pslogin.setString(1,userid);
		        		  pslogin.setString(2, userpass);
		        		  rs=pslogin.executeQuery();
		        		  
		        		  
		        		  
		        		  if(rs.next())
		        		  {
		        			  int timeout=1*6000;  
		        			  
		        			  HttpSession session=request.getSession();
		        			  System.out.println(session.getId()+"in login"); 
		        		 
				        	  session.setAttribute("sessionkey", userid);
				        	
				        	  session.setMaxInactiveInterval(timeout);
				        	  
				        	  
				        	  
				        	  
				        	  
				        	  
				        	  
				        	  String role=rs.getString("usertype");
				        	  session.setAttribute("roletype", role);
				        	  if(role.equals("admin"))
				        		  response.sendRedirect("/HostelFinder/jsp/userhome.jsp");
				        	  
				        	  if(role.equals("user"))
				        		  response.sendRedirect("/HostelFinder/jsp/userhome.jsp");
				        		  
				        	  
				        	  if(role.equals("owner"))
				        		  response.sendRedirect("/HostelFinder/jsp/owner/ownerhome.jsp");
				        		  
				        		  
		        			  
		        		  }
		        		  else {
		 		        	 
		        				 
		        			        	 request.setAttribute("msg", ErrorMessage.LOGINMESSAGE);
		        			        	 RequestDispatcher rd=request.getRequestDispatcher("/jsp/index.jsp");
		        			        	 rd.forward(request, response);
		        			        	 
		        			         }
		        			
		        		  
		        		  
		        	  } 
		        	  catch(SQLException se)
		        	  {
		        		System.out.println(se);  
		        		  
		        	  }
		 
		         
		
	}

	}
