package ehotel.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ehotel.dbtask.CrudOperation;

import java.sql.*;
/**
 * Servlet implementation class Compose
 */
@WebServlet("/Compose")
public class Compose extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private static Connection con ;
	private static PreparedStatement ps ;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Compose() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String	rid=request.getParameter("txtreciverid");
		String	subject=request.getParameter("txtsubject");
		String	msg=request.getParameter("txtmsg");
		
		java.util.Date d= new java.util.Date();
		java.sql.Date sd=new java.sql.Date(d.getTime());
		
		 HttpSession hs=request.getSession(false);
		 String role=(String)hs.getAttribute("roletype");
		 String  id=(String) hs.getAttribute("sessionkey");
		
		
		
		
		System.out.println(rid+"  "+msg+"  "+"  "+subject+"  "+id+"  "+sd+role);
		

		try
		{
			String strinsert="insert into message(senderid, receiverid, subject, message, date, rstatus, sstatus) values(?,?,?,?,?,?,?)";
			 con= CrudOperation.createConnection();
			ps=con.prepareStatement(strinsert);
			ps.setString(1,id);
			ps.setString(2,rid);
			ps.setString(3,subject);
			ps.setString(4,msg);
			ps.setDate(5,sd);
			ps.setString(6,"true");
			ps.setString(7,"true");
			System.out.println(ps);
			
			int rw=ps.executeUpdate();
	if(rw>0)
			{
				 
				if(role.equals("user"))
				{
					response.sendRedirect("/HostelFinder/jsp/userhome.jsp");
					
				}
				if(role.equals("owner"))
				{
					response.sendRedirect("/HostelFinder/jsp/owner/ownerhome.jsp");
				}

				
			}
			
			
		}
		catch(SQLException se)
		{
			System.out.println(se);
			
		}
		}
	}

