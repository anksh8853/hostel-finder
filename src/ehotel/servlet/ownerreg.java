package ehotel.servlet;

import java.io.IOException;
import java.sql.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ehotel.dbtask.CrudOperation;

/**
 * Servlet implementation class ownerreg
 */
@WebServlet("/ownerreg")
public class ownerreg extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Connection con;
	private PreparedStatement pslogin,psreg;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ownerreg() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		  
		
		String ownerid=request.getParameter("txtowid"); 
		 String userpass=request.getParameter("txtowpass"); 
		 
		 String username=request.getParameter("txtname"); 
		 String address=request.getParameter("txtaddress");
		  String email=request.getParameter("txtemail"); 
		  String phone=request.getParameter("txtphone");
		  String ownername=request.getParameter("txtowname");
		  
		   
		  java.util.Date d=new java.util.Date();
			java.sql.Date sd=new java.sql.Date(d.getTime());
		  
		  try {
				con=CrudOperation.createConnection();
				con.setAutoCommit(false);
				
				 
				
				String strlogin="insert into logindetails values(?,?,?)";
				String strreg="insert into ownerreg(ownerid, address, email, phone, dor, ownername) values(?,?,?,?,?,?)";
				
				pslogin=con.prepareStatement(strlogin);
				pslogin.setString(1, ownerid);
				pslogin.setString(2, userpass);
				pslogin.setString(3, "owner");
				int rwlogin=pslogin.executeUpdate();
				
				
				
				psreg=con.prepareStatement(strreg);
				psreg.setString(1, ownerid);
				 
				psreg.setString(2, address);
				psreg.setString(3, email);
				psreg.setString(4, phone);
				psreg.setDate(5, sd);
				psreg.setString(6, ownername);
			 
				
				System.out.println(psreg);
				
				int  rwreg=psreg.executeUpdate();
				if(rwlogin >0 && rwreg >0)
				{
					con.commit();
					response.sendRedirect( "/HostelFinder/jsp/index.jsp");
					
					
					
				}
				 
				
			}
			
			catch (SQLException se) {
				 System.out.println(se);
			}
			
			
			finally {
				
				try {
				if(pslogin!=null)pslogin.close();
				if(psreg!=null)psreg.close();
				if(con!=null)con.close();
				
				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}
			 
			}
		
			





	}


