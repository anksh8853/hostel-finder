package ehotel.servlet;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class LogOff
 */
@WebServlet("/LogOff")
public class LogOff extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LogOff() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		HttpSession hs=request.getSession(false); 
		
		  
		 long time =hs.getLastAccessedTime();
		 Calendar cl=Calendar.getInstance(); 
	     cl.setTimeInMillis(time);
	     int minute=cl.get(Calendar.MINUTE);
	     int hour=cl.get(Calendar.HOUR);
	     int seconds=cl.get(Calendar.SECOND);
	     int day=cl.get(Calendar.DAY_OF_MONTH);

	     
	     
	
		System.out.println("hour is"+hour+"minute is"+minute+"second is"+seconds);
		
		System.out.println("day is"+day);
		
		
		if(hs!=null)
		{
			
		hs.removeAttribute("sessionkey");
		hs.invalidate();//destroy the existing session
		
		
		response.sendRedirect("/HostelFinder/jsp/index.jsp");
	}
		else
	              	{
			      response.sendRedirect("/HostelFinder/jsp/index.jsp");
	                 	}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
