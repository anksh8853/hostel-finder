package ehotel.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ehotel.dbtask.CrudOperation;

/**
 * Servlet implementation class Registration
 */
@WebServlet("/Registration")
public class Registration extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Connection con;
	private PreparedStatement pslogin,psreg;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Registration() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType("text/html");
			PrintWriter out=response.getWriter();
			
		 String uid=request.getParameter("userid");
		 if(uid.equals("scott"))
		 {
			 
			 out.println("userid already exist");
			 
			 
		 }
		 else {
			 out.print("not");
		 }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		
		
		  String userid=request.getParameter("textid"); 
		  String userpass=request.getParameter("textpass"); 
		  String name=request.getParameter("txtusername"); 
		  String email=request.getParameter("txtuseremail");
		  String address=request.getParameter("txtuseraddress");
		  String gender=request.getParameter("rdgender");
		   String phone=request.getParameter("txtuserphone");
		 
		 
		System.out.println(userid+userpass+name+gender+address+email+phone); 
		  
		  
		  
		 
	 
	
	
	java.util.Date d=new java.util.Date();
	java.sql.Date sd=new java.sql.Date(d.getTime());
	
	
	try {
		con=CrudOperation.createConnection();
		con.setAutoCommit(false);
		
		
		
		String strlogin="insert into logindetails values(?,?,?)";
		String strreg="insert into registration(Userid, Name, Address, Email, Gender, Phone, Dor) values(?,?,?,?,?,?,?)";
		
		
	 
		
		
		
		pslogin=con.prepareStatement(strlogin);
		pslogin.setString(1, userid);
		pslogin.setString(2, userpass);
		pslogin.setString(3, "user");
		int rwlogin=pslogin.executeUpdate();
		
		
		
		psreg=con.prepareStatement(strreg);
		psreg.setString(1, userid);
		psreg.setString(2, name);
		psreg.setString(3, address);
		psreg.setString(4, email);
		psreg.setString(5, gender);
		psreg.setString(6, phone);
		psreg.setDate(7, sd);
		
		System.out.println(psreg);
		
		int  rwreg=psreg.executeUpdate();
		if(rwlogin >0 && rwreg >0)
		{
			con.commit();
			response.sendRedirect( "/HostelFinder/jsp/index.jsp");
			
			
			
		}
		 
		
	}
	
	catch (SQLException se) {
		 System.out.println(se);
	}
	
	
	finally {
		
		try {
		if(pslogin!=null)pslogin.close();
		if(psreg!=null)psreg.close();
		if(con!=null)con.close();
		
		}
		catch (Exception e) {
			 
		}
	}
 
	}
}
	














