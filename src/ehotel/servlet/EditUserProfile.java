package ehotel.servlet;

import java.io.IOException;
import java.sql.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ehotel.dbtask.CrudOperation;

/**
 * Servlet implementation class EditUserProfile
 */
@WebServlet("/EditUserProfile")
public class EditUserProfile extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Connection con;
	private PreparedStatement ps;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditUserProfile() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		 String phone=request.getParameter("txtphone");
		 
		 String email=request.getParameter("txtemail");
		 String address=request.getParameter("txtadd");
		 
		 HttpSession hs=request.getSession(false);
		 String id=(String) hs.getAttribute("sessionkey");
		 con=CrudOperation.createConnection();
		 
		 
		 try {
			 String strupdate="update registration set email=?,address=?,phone=? where userid=?";
			 ps=con.prepareStatement(strupdate);
			 ps.setString(1, email);
			 ps.setString(2, address);
			 ps.setString(3, phone);
		 
			 ps.setString(4, id);
			 int rw=ps.executeUpdate();
			 if(rw>0)
				 response.sendRedirect("/HostelFinder/jsp/userprofile.jsp");
		 }
		 catch (SQLException se) 
		 {
			 System.out.println(se);
			 
			   
	}

}
}
