package ehotel.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ehotel.dbtask.CrudOperation;

/**
 * Servlet implementation class AddHstl
 */
@WebServlet("/AddHstl")
public class AddHstl extends HttpServlet {
	private static final long serialVersionUID = 1L;
	PreparedStatement ps;
	Connection con;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddHstl() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession hs=request.getSession(false);
    	String id=(String)hs.getAttribute("sessionkey");

		String hname=request.getParameter("txthname");
		String phone=request.getParameter("txtphone");
		String city=request.getParameter("cmbcity");
		String area=request.getParameter("txtarea");
		String category=request.getParameter("category");
		String forg=request.getParameter("for");
		String address=request.getParameter("txtaddress");
		
		try {
			con=CrudOperation.createConnection();
		 
			
			String strin="insert into hosteldetails(ownerid, hname, phone, cityid, area, category, forg, address) values(?,?,?,?,?,?,?,?)";
		 
			
			
			ps=con.prepareStatement(strin);
			ps.setString(1, id);
			ps.setString(2, hname);
			ps.setString(3, phone);
			ps.setString(4, city);
			ps.setString(5, area);
			ps.setString(6, category);
			ps.setString(7, forg);
			ps.setString(8, address);
			System.out.println(ps);
			int rw=ps.executeUpdate();
			
			if(rw>0 ) {
				response.sendRedirect("/HostelFinder/jsp/owner/addroom.jsp");
			}
			
		}catch(SQLException se) {
			System.out.println(se);
		}
		finally {
			try {
				if(ps!=null)ps.close();
			}catch(SQLException se) {
				System.out.println(se);
			}
		}
	}

}
