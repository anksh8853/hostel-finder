package ehotel.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ehotel.dbtask.CrudOperation;

/**
 * Servlet implementation class addnews
 */
@WebServlet("/addnews")
public class addnews extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Connection con;
	private PreparedStatement ps;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public addnews() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		  String title=request.getParameter("title");
		  String content=request.getParameter("contents");
		  
		  
		  java.util.Date d=new java.util.Date();
		  java.sql.Date sd=new java.sql.Date(d.getTime());
		  
		  try {
			  con=CrudOperation.createConnection();
			  String strinsert="insert into news(title , contents , date) values(?,?,?)";
			  ps=con.prepareStatement(strinsert);
			  ps.setString(1, title);
			  ps.setString(2, content);
			  
			  ps.setDate(3, sd);
			  System.out.println(ps);
			 int rw= ps.executeUpdate(); //insert update delete ----it return integer only
			  
			  if(rw>0)
			  System.out.println("news added");
			  
		  }
		  catch (SQLException se)
		  {
			 System.out.println(se);
		}
		  finally {
			  
			  if(ps!=null)
				  try {
					  ps.close();
					  
					  
				  }catch (SQLException e) {
					 e.printStackTrace();
				}
		  }
	}

}
