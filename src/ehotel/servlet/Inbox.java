package ehotel.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.*;
import ehotel.dbtask.CrudOperation;

/**
 * Servlet implementation class Inbox
 */
@WebServlet("/Inbox")
public class Inbox extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 private Connection con ;
     private PreparedStatement ps; 
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Inbox() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String[] msgarray=request.getParameterValues("chkdelete");
			con=CrudOperation.createConnection();
			
			try {
				con.setAutoCommit(false);
				String strupdate="update  message  set  rstatus='false' where  msgid=?";
				
				ps=con.prepareStatement(strupdate);
			for(int i=0;i<msgarray.length;i++)
			{
				
				
				ps.setInt(1, Integer.parseInt(msgarray[i]));
				ps.addBatch();
			}
			
		int flag=0;
		int []rw=	ps.executeBatch();
		for(int i=0;i<rw.length;i++)
		{
			if(rw[i]<0)
			{
				flag=1;
				break;
			}
		}
		if(flag==0)
		{
			con.commit();
			response.sendRedirect("/HostelFinder/jsp/inbox.jsp");
			
		}
			}
			catch(SQLException se)
			{
				
				
				System.out.println(se);
			}

			finally {
				try {
					if(ps!=null)ps.close();
					 
					if(con!=null) con.close(); } 
				catch (SQLException e) 
				{ 
				System.out.println(e);
				}
	}

}}
