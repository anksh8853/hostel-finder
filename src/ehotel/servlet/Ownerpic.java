package ehotel.servlet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import ehotel.dbtask.CrudOperation;

/**
 * Servlet implementation class UploadPic
 */
@WebServlet("/Ownerpic")
@MultipartConfig
public class Ownerpic extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Connection con;
	PreparedStatement ps;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Ownerpic() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		// response.setContentType("text/html;charset=UTF-8");
			
		 ServletContext sc=getServletContext();
		String path=sc.getRealPath("/"); //return the absolute path to website
		 
		System.out.println(path);



	HttpSession hs=request.getSession(false);
	String u_id=(String)hs.getAttribute("sessionkey");
		
	String newpath=path+u_id;
		File f=new File(newpath);
		if(!f.exists())
		{
			
			f.mkdir();
			System.out.println("directorycreated");
		}
		
		
		
		/*
		 * String description = request.getParameter("txtdesc");
		 * System.out.println(description);
		 */
		
	    final Part filePart = request.getPart("fileupload"); // file upload control name
	    	final String fileName = getFileName(filePart);
	    	
	    	System.out.println("file name is" +fileName);
	    	
	    	
           String strupdate="update ownerreg set pic=? where ownerid=?";
	   
	   
	   try{
		   con=CrudOperation.createConnection();
		   ps=con.prepareStatement(strupdate);
		   ps.setString(1, fileName);
		   ps.setString(2, u_id);
		   System.out.println(ps);
		   int rw=ps.executeUpdate();
		   if(rw>0)
		   {
			   
		   System.out.println("picadded");
			   response.sendRedirect("/HostelFinder/jsp/owner/ownerhome.jsp");
		   }
		   
	   }
	    	catch(SQLException se)
	   {
	    		
	    		
	    		System.out.println(se);
	   }
	    
	   System.out.println(fileName);

	    OutputStream out = null;
	    InputStream filecontent = null;
	    final PrintWriter writer = response.getWriter();

	    try {
	        out = new FileOutputStream(new File(newpath + File.separator
	                + fileName));
	        filecontent = filePart.getInputStream();

	        int read = 0;
	        final byte[] bytes = new byte[1024];

	        while ((read = filecontent.read(bytes)) != -1) {
	            out.write(bytes, 0, read);
	        }
	 System.out.println("New file " + fileName + " created at " + newpath);
	        
	    } catch (FileNotFoundException fne) {
	       
	        System.out.println("<br/> ERROR: " + fne.getMessage());

	       
	    } finally {
	        if (out != null) {
	            out.close();
	        }
	        if (filecontent != null) {
	            filecontent.close();
	        }
	        if (writer != null) {
	            writer.close();
	        }
	    }
	}

	
		
		
	

private String getFileName(final Part part) {
	
	
	
	
    final String partHeader = part.getHeader("content-disposition");
   
    for (String content : part.getHeader("content-disposition").split(";")) {
        if (content.trim().startsWith("filename")) {
            return content.substring(
                    content.indexOf('=') + 1).trim().replace("\"", "");
        }
    }
    return null;
}

	
}
